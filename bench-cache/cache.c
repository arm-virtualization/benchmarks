#include "cache.h"

#include <stdlib.h>
#include <time.h>
#include <stdint.h>

#include "chrono_utils.h"

void bench_1d_array (int size) {
	int *c = (int *) malloc (sizeof (int) * size);

	if (c == NULL) {
		printf ("memory allocation error\n");
		exit (EXIT_FAILURE);
	}
	
	for (int i = 0; i < 0xff; i++)
		for (int j = 0; j < size; j++)
			c[j] = i*j;
	
	free (c);
}

void test_cache_size (int read_iter, int array_size,  struct timespec *exec_time) {
	uint32_t *buffer;
	int *indices; // list of indices used to read the array
	
	uint32_t read_val;
	
	struct timespec start, end;
	
	// init random generator
	srand(time(NULL));
	
	buffer = (uint32_t *) malloc (sizeof (uint32_t) * array_size);
	if (buffer == NULL) {
		printf ("memory allocation error\n");
		exit (EXIT_FAILURE);
	}

	indices = (int *) malloc (sizeof (int) * array_size);
	if (indices == NULL) {
		printf ("memory allocation error\n");
		exit (EXIT_FAILURE);
	 }

	// write in array to be sure its initilized
	for (int i = 0; i < array_size; i++) {
		buffer[i] = i+1000/(i+1);
		//indices[i] = i;
		indices[i] = rand() % array_size;
	}

	clock_gettime (CLOCK_MONOTONIC, &start);
	
	// read our array read_iter times
	// if the array fits in cache, will benefit here
	for (int k = 0; k < read_iter; k++) {
		for (int i = 0; i < array_size; i++) {
			// randomize the reads to benefits from big cache
			//int rid = rand() % array_size;
			read_val = buffer[indices[i]];
			//read_val = buffer[i];
		}
	}
	
	clock_gettime (CLOCK_MONOTONIC, &end);
	time_diff (&start, &end, exec_time);
	
	free (buffer);
	free (indices);
}


void random_number_generation (int nb_rand) {
	int tmp_rand; // generated number
	
	// init random generator
	srand(time(NULL));
	
	for (int i = 0; i < nb_rand; i++) {
		tmp_rand = rand();
	}
	
}
