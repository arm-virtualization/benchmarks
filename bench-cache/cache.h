#ifndef CACHE_H
#define CACHE_H

// forward declaration
struct timespec;

/**
 * Benchmark 1d array operation
 */
void bench_1d_array (int size);


/** 
 * Benchmark the time taken to generate random numbers
 */
void random_number_generation (int nb_rand);

/**
 * 
 */
void test_cache_size (int read_iter, int array_size, struct timespec *exec_time);
#endif
