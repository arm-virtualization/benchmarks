#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdint.h>

#include "chrono_utils.h"
#include "cache.h"

// forward declarations
static void print_help (char **argv);
static void print_infos ();
static void exec_cache_size ();
static void exec_array_1d (int argc, char **argv);
static void exec_random ();


int main (int argc, char **argv) {
	// Check help argument
	if (argc == 2 && strcmp (argv[1], "-h") == 0) {
		print_help (argv);
		exit (EXIT_SUCCESS);
	}
	
	print_infos();

	exec_random ();
	
	exec_array_1d (argc, argv);
	
	exec_cache_size ();
}


static void print_help (char **argv) {
	printf ("Usage :\n");
	printf ("\t %s \n", argv[0]);
	printf ("\t %s 1d_array_size\n", argv[0]);
}

static void print_infos () {
	printf ("Platform information\n");
	printf ("--------------------------\n");
	printf ("int size\t:%zu\n", sizeof(int));
	printf ("uint32_t size\t:%zu\n", sizeof(uint32_t));
	printf ("\n\n");
}


static void exec_random () {
	// start and end time for measurement
	struct timespec start, end, diff;
	
	int start_rnum = 100;
	int max_rnum = 10000;
	
	printf ("Random number generation\n");
	printf ("--------------------------\n");
	printf ("nb_rand\ttime[s]\n");
	
	for (int i = start_rnum; i < max_rnum; i *= 2) {
		clock_gettime (CLOCK_MONOTONIC, &start);
		random_number_generation (i);
		clock_gettime (CLOCK_MONOTONIC, &end);
		time_diff (&start, &end, &diff);
		printf ("%d\t%lu.%lu\n", i, diff.tv_sec, diff.tv_nsec);
	}
	

}




static void exec_array_1d (int argc, char **argv) {
	// start and end time for measurement
	struct timespec start, end, diff;
	
	int size = 0;
	
	// size value can also be given as argument
	if (argc == 2)
		size = atoi (argv[2]);
	else
		size = 5*1024*1024; // Allocate 20M of memory of 4-bytes integer 


	printf ("\n");
	printf ("1D array\n");
	printf ("--------------------------\n");
	printf ("size\ttime[s]\n");
	clock_gettime (CLOCK_MONOTONIC, &start);
	bench_1d_array (size);
	clock_gettime (CLOCK_MONOTONIC, &end);
	time_diff (&start, &end, &diff);
	printf ("%d\t%lu.%lu\n", size, diff.tv_sec, diff.tv_nsec);
}

static void exec_cache_size () {
	int size = 32;
	int read_iter= 1000;
	
	struct timespec diff;
	
	printf("\n");
	printf ("Test cache size\n");
	printf ("--------------------------\n");
	printf ("r_iter\tsize\ttime[s]\n");
	

	do {
		test_cache_size (read_iter, size, &diff);
		printf ("%d\t%d\t%lu.%lu\n", read_iter, size, diff.tv_sec, diff.tv_nsec);
		size *= 2;
	}
	while (size < 80*1024*1024); // 320M of 4-bytes integer 
}
