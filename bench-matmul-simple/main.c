/*
 * Very simple matrix multiplication program.
 * Size of 2D square matrix of int defined by ARRAY_SIZE
 * 
 */


/* (800*800) * 4 * 3 => 7.32MB of memory used on the stack */
/* 2d matrix  int  3 matrices */
#define ARRAY_SIZE 800 

int main (int argc, char **argv) {

	int i,j,k;
	int a[ARRAY_SIZE][ARRAY_SIZE];
	int b[ARRAY_SIZE][ARRAY_SIZE];
	int c[ARRAY_SIZE][ARRAY_SIZE];
	
	// sets values in the matrices
	for (i = 0; i < ARRAY_SIZE; ++i) {
		for (j = 0; j < ARRAY_SIZE; ++j) {
			a[i][j] = i+j;
			b[i][j] = i+j;
			c[i][j] = i+j;
		}
	}
	
	// compute !
	for (i = 0; i < ARRAY_SIZE; i++) {
		for (j = 0; j < ARRAY_SIZE; j++) {
			int sum = 0;
			for (k = 0; k < ARRAY_SIZE; k++) {
				sum += a[i][k] * b[k][j];
			}
		c[i][j] = sum;
		}
	}
	
	return 0;
}
