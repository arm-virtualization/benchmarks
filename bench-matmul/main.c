#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "chrono_utils.h"
#include "matmul.h"

int main (int argc, char **argv) {
	
	// default matrix size is 800
	int mat_size = 800;
	
	// start and end time for measurement
	struct timespec start, end, diff;
	
	// size value can also be given as argument
	if (argc == 2) {
		mat_size = atoi (argv[1]);
	}	
	
	clock_gettime (CLOCK_MONOTONIC, &start);
	
	bench_matmul (mat_size);
	
	clock_gettime (CLOCK_MONOTONIC, &end);
	
	time_diff (&start, &end, &diff);
	
	printf ("Time taken: %lu.%lu s\n", diff.tv_sec, diff.tv_nsec);
	
	return 0;
}
