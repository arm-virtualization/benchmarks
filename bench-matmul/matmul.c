#include "matmul.h"

#include <stdlib.h>

// forward declaration
static void initialize_matrix (int **a, int m, int n);
static int** allocate_matrix (int m, int n);
static void deallocate_matrix (int **a, int m);


void bench_matmul (int mat_size) {
	int i,j,k;
	const int m=mat_size, n=mat_size, p=mat_size;
	int **a = allocate_matrix (m, n);
	int **b = allocate_matrix (n, p);
	int **c = allocate_matrix (m, p);
	
	initialize_matrix (a, m, n);
	initialize_matrix (b, n, p);
	initialize_matrix (c, m, p);
	
	for (i = 0; i != m; i++) {
		for (j = 0; j != n; j++) {
			int sum = 0;
			for (k = 0; k != p; k++) {
				sum += a[i][k] * b[k][j];
			}
		c[i][j] = sum;
		}
	}
	
	deallocate_matrix (a, m);
	deallocate_matrix (b, n);
	deallocate_matrix (c, m);
	
	return;
}

/*
 * Fill in a matrix a with elements.
 * No randomization here - generates always the same matrix elements
 * The size of a is [m][n]
 */
static void initialize_matrix (int **a, int m, int n) {
	int i,j;
	
	for (i = 0; i != m; ++i) {
		for (j = 0; j != n; ++j) {
			a[i][j] = i+j;
		}
	}
}

static int** allocate_matrix (int m, int n) {
	int i;
	int **a = (int**) malloc (m * sizeof (int*));

	if (a == NULL) {
		printf ("memory allocation error\n");
		exit (EXIT_FAILURE);
	 }

	for (i = 0; i < m; i++ ) {
		a[i] = (int*) malloc (n * sizeof (int));
	}
	
	return a;
}

static void deallocate_matrix (int **a, int m) {
	int i;
	
	for (i = 0; i < m; i++ ) {
		free (a[i]);
	}

	free (a);
}
