#ifndef MATMUL_H
#define MATMUL_H

/*
 * Benchmark a matrix multiplication operation.
 * Will create three square matrices of size mat_size
 */
void bench_matmul (int mat_size);

#endif
