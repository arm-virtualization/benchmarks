#include "bench_memory.h"

#include <stdlib.h>
#include <stdio.h>

void memory_allocation_with_init (struct memory_region_bench *mem_reg) {
	int size = 1024 * 1024 * 8; // 32 MB
	int i;
	int *mem;

	mem = (int *) malloc (size * sizeof (int));
	if (mem == NULL) {
		perror ("memory allocation failed");
		exit (EXIT_FAILURE);
	}
	
	for (i = 0; i < size; i++) {
		mem[i] = i;
	}
	
	mem_reg->mem = mem;
	mem_reg->size = size;
	mem_reg->initialized = 1;
}


void memory_read (struct memory_region_bench *mem_reg) {
	
	int i, j;
	int *mem = mem_reg->mem;
	int size = mem_reg->size;
	
	if (! mem_reg->initialized) {
		printf ("Memory not initialized");
		return;
	}
		
	for (i = 0; i < size; i++) {
		j += mem[i];
	}
	
}

void memory_write (struct memory_region_bench *mem_reg) {
	int i;
	int *mem = mem_reg->mem;
	int size = mem_reg->size;
	
	if (! mem_reg->initialized) {
		printf ("Memory not initialized");
		return;
	}
	
	for (i = 0; i < size; i++) {
		mem[i] = 5;
	}
	
}

void memory_free (struct memory_region_bench *mem_reg) {
	if (! mem_reg->initialized)
		return;
		
	free (mem_reg->mem);
}
