#ifndef BENCH_MEMORY_H
#define BENCH_MEMORY_H

struct memory_region_bench {
	int initialized;
	int size;
	int *mem;
};

void memory_allocation_with_init (struct memory_region_bench *mem_reg);

void memory_read (struct memory_region_bench *mem_reg);

void memory_write (struct memory_region_bench *mem_reg);

void memory_free (struct memory_region_bench *mem_reg);

#endif
