#ifndef CHRONO_UTILS_H
#define CHRONO_UTILS_H

struct timespec;

int time_diff (struct timespec* start, struct timespec* end, struct timespec* diff);

#endif
