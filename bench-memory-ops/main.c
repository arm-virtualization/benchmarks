#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "chrono_utils.h"
#include "bench_memory.h"

// forward declaration
static void bench_allocation_with_init (struct memory_region_bench *mem_reg);
static void bench_read (struct memory_region_bench *mem_reg);
static void bench_write (struct memory_region_bench *mem_reg);
static void bench_free (struct memory_region_bench *mem_reg);


int main (int argc, char **argv) {

	struct memory_region_bench memory_region;
	
	/*
	// max value can also be given as argument
	if (argc == 2) {
		max = atoi (argv[1]);
	}	
	*/
	
	bench_allocation_with_init (&memory_region);
	bench_read (&memory_region);
	bench_write (&memory_region);
	bench_free (&memory_region);
	
	return 0;
}


static void bench_allocation_with_init (struct memory_region_bench *mem_reg) {
	// start and end time for measurement
	struct timespec start, end, diff;
	
	clock_gettime (CLOCK_MONOTONIC, &start);
	
	memory_allocation_with_init (mem_reg);
	
	clock_gettime (CLOCK_MONOTONIC, &end);
	time_diff (&start, &end, &diff);
	printf ("Memory alloc and init: %lu.%lu s\n", diff.tv_sec, diff.tv_nsec);
}


static void bench_read (struct memory_region_bench *mem_reg) {
	// start and end time for measurement
	struct timespec start, end, diff;
	int i;
		
	clock_gettime (CLOCK_MONOTONIC, &start);
	
	for (i = 0; i < 100; i++) {
		memory_read (mem_reg);
	}
	
	clock_gettime (CLOCK_MONOTONIC, &end);
	time_diff (&start, &end, &diff);
	printf ("Memory read: %lu.%lu s\n", diff.tv_sec, diff.tv_nsec);
}


static void bench_write (struct memory_region_bench *mem_reg) {
	// start and end time for measurement
	struct timespec start, end, diff;
	int i;
		
	clock_gettime (CLOCK_MONOTONIC, &start);
	
	for (i = 0; i < 100; i++) {
		memory_write (mem_reg);
	}
	
	clock_gettime (CLOCK_MONOTONIC, &end);
	time_diff (&start, &end, &diff);
	printf ("Memory write: %lu.%lu s\n", diff.tv_sec, diff.tv_nsec);
}

static void bench_free (struct memory_region_bench *mem_reg) {
	// start and end time for measurement
	struct timespec start, end, diff;
		
	clock_gettime (CLOCK_MONOTONIC, &start);

	memory_free (mem_reg);
	
	clock_gettime (CLOCK_MONOTONIC, &end);
	time_diff (&start, &end, &diff);
	printf ("Memory free: %lu.%lu s\n", diff.tv_sec, diff.tv_nsec);
}

