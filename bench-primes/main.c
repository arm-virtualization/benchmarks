#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "chrono_utils.h"
#include "prime.h"

int main (int argc, char **argv) {
	// max number to test (default value)
	unsigned int max=100000;
	
	unsigned int cnt_primes;
	
	// start and end time for measurement
	struct timespec start, end, diff;
	
	// max value can also be given as argument
	if (argc == 2) {
		max = atoi (argv[1]);
	}	
	
	clock_gettime (CLOCK_MONOTONIC, &start);
	
	cnt_primes = bench_primes (max);
	
	clock_gettime (CLOCK_MONOTONIC, &end);
	
	time_diff (&start, &end, &diff);
	
	printf ("Found %d primes\n", cnt_primes);
	printf ("Time taken: %lu.%lu s\n", diff.tv_sec, diff.tv_nsec);
	
	return 0;
}
