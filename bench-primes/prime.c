#include "prime.h"

#include <stdlib.h>

unsigned int bench_primes (unsigned int max) {
	
	// current number to test
	unsigned int nb=1;
	
	// nested loop iterator
	unsigned int i=2;
	
	// counter for found prime numbers
	unsigned int cnt_primes = 0;
		
	while (nb <= max) {
		i=2;
		
		while (i <= nb) {
			if (nb % i == 0) {
				// is not prime
				break;
			}
			i++;
		}
		
		// nb is prime if we checked with all i's
		if (nb == i) {
			cnt_primes++;
		}
		nb++;		
	}
	
	return cnt_primes;
}
