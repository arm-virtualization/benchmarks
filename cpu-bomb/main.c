#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

// foward declaration

void bomb (int nb_procs);

int main (int argc, char **argv) {
	// default is one process 
	int nb_procs = 1;
	
	// number of processes can be passed as arg
	if (argc == 2) {
		nb_procs = atoi(argv[1]);
	}	
	
	bomb (nb_procs);
	
	return 0;
}


void bomb (int nb_procs) {
	pid_t *pid = (pid_t*) malloc (nb_procs * sizeof(pid_t));
	int status = 0;
	
	for (int i=0; i<nb_procs; i++) {
		pid[i] = fork();
		if (pid[i] == -1)
		{
			// error
			printf("can't fork, error %d\n", errno);
			exit(EXIT_FAILURE);
		}
		else if (pid[i] == 0)
		{
			// child: the bomb
			while(1);
			_exit(0);
		}
	}
	
	// wait for all children
	for (int i=0; i<nb_procs; i++) {
		wait(&status);
	}
	
	free(pid);
}
